import React, { Component } from 'react';
import Products from './Products';
import Sidebar  from './Sidebar';

class Login extends Component {


  render() {
      
      return (
        <div className="login page">
          <h3>Log in</h3>
            <form className="form-inline">
                <div className="form-group mb-2">
                    <label htmlFor="staticEmail2" className="sr-only">Email</label>
                    <input type="text" readOnly className="form-control-plaintext" id="staticEmail2" value="email@example.com"/>
                </div>
                <div className="form-group mx-sm-3 mb-2">
                    <label htmlFor="inputPassword2" className="sr-only">Password</label>
                    <input type="password" className="form-control" id="inputPassword2" placeholder="Password"/>
                </div>
                <button type="submit" className="btn btn-primary mb-2">Confirm identity</button>
            </form>
        </div>
      );

  }


}

export default Login;
