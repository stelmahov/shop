import React, { Component } from 'react';
import Products from './Products';
import {getProductsAction} from '../actions/app';
import Sidebar  from './Sidebar';
import {connect} from 'react-redux';

class MainProduct extends Component {

    constructor(props){

        super(props);

        this.state = {
            product:{},
            slug:this.props.match.params.slug
        }
    }

    componentDidMount()
    {
        if(this.props.products.length === 0){
            this.props.getProducts(this.setProduct)
        }else{
            this.setProduct();
        }
    }

    setProduct = () => 
    {

        let slug = this.state.slug;

        for(let j in this.props.products){
            if(this.props.products[j]['slug'] === slug){
                this.setState({product:this.props.products[j]});
            }
        }
    }

    add = (el) => {
        this.props.addCart(this.state.product);
    }


    render() {
        
        return (
            <div className="product row mt-3 mb-3">
                <div className="image col-sm-6">
                    <img className="img-fluid" src={'/img/'+this.state.product.image} alt="img" />
                </div>
                <div className="col-sm-6">
                    <h3>{this.state.product.name}</h3>
                    <div className="price">
                        {this.state.product.price}
                    </div>
                    <div className="manufacturer">
                        {this.state.product.manufacturer}
                    </div>
                    <div className="description">
                        {this.state.product.description}
                    </div>
                    {
                    this.state.product.quantity 
                    ? <button onClick={this.add} type="button" className="btn btn-primary">Add to cart</button>
                    : <button type="button" className="btn btn-success">Pre order</button>
                    }
                </div>
            </div>

        );

    }


}

export default connect (
    state => ({
      products: state.app.products
    }),
    dispatch => ({
        getProducts: (callback) => {
            dispatch(getProductsAction(callback));
        },
        addCart: (Product) => {
            dispatch({type: 'ADD_CART', data: Product})
        }
    })
  )(MainProduct);
