export const getProductsAction = (callback) => dispatch => 
{
    fetch("http://localhost:3006/products", {
        method:'POST'
    }).then((response) =>
    {
        return response.json()
    }).then((d) =>
    {
        dispatch({type:'SET_PRODUCTS', data:d});
        callback ? callback() : '';
    });
}

export const purchaseAction = (products) => dispatch => 
{
    fetch("http://localhost:3006/purchase", {
        method:'POST',
        body:JSON.stringify(products)
    }).then((response) =>
    {
        return response.json()
    }).then((d) =>
    {
        console.log(d);
    });
}