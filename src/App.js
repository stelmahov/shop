import React, { Component } 		from 'react';
import Header    					from './components/Header';
import Home   						from './components/Home';
import Registration   				from './components/Registration';
import Login   						from './components/Login';
import About   						from './components/About';
import Products   					from './components/Products';
import MainProduct   				from './components/MainProduct';
import Footer    					from './components/Footer';
import {Switch, Route, Redirect} 	from 'react-router-dom';

import './App.css';

class App extends Component {

	render() {
		return (
			<div className="App container">
				<Header />

					<Switch>
						<Route path='/home' component={Home}/>
						<Route path='/products' component={Products}/>
						<Route path='/product/:slug' component={MainProduct}/>
						<Route path='/registration' component={Registration}/>
						<Route path='/login' component={Login}/>
						<Route path='/about-us' component={About}/>
						<Redirect from='/' to='/home'/>
					</Switch>

				<Footer />
			</div>
		);
	}


}

export default App;